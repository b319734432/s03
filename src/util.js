const names = {
	"Brandon": {
		"name": "Brandon Boyd",
		"age": 35,
		"username": "boydbrandon"
	},
	"Steve": {
		"name": "Style Tyler",
		"age": 56,
		"username": "styler"
	}
}

module.exports = {
	names: names
}
